import webapp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h2>Page not found {resource}. Ingrese su mensaje</h2>

    <form method="post">
        <input type="text" name="mensaje" required>
        <input type="submit" value="Enviar">
    </form>
</body>

</html>
"""

class ContentpostApp(webapp.webApp): #heredo el contenido de webapp aqui, que es el init por que el resto no hace nada, es decir me olvido de los sockets

    contents = {'/': "<p>Welcome to the main page</p>",
                '/hello': "<p>Hello, people</p>",
                '/bye': "<p>Bye all!!</p>",
                '/pepito': "<p> Esta es la pagina de pepito </p>"}

    def parse (self, request):  #SELF INDICA QUE USAMOS LA VARIABLE QUE ESTA DENTRO DEL CONTENT, EN ESTE CASO CONTENTAPP
        """Return the resource name"""
        #data = {}

        metodo = request.split(' ',2)[0]
        #data['metodo'] = metodo

        recurso = request.split(' ',2)[1]
        #data['recurso'] = recurso

        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            body = None
        else:
            body = request[body_start:]

        return (metodo, recurso, body)

    def process(self, parsedRequest):
        """Produce the page with the content for the resource"""

        (metodo, recurso, body) = parsedRequest

        if metodo == 'POST':
            mensaje = body.split('=')[1]
            mensaje = mensaje.replace('+',' ')
            self.contents[recurso] = mensaje
            page = PAGE.format(content=mensaje)
            code = '200 OK'

        if metodo == 'GET':
            if recurso in self.contents:
                content = self.contents[recurso]
                page = PAGE.format(content=content)
                code = "200 OK"
            else:
                page = PAGE_NOT_FOUND.format(resource=recurso)
                code = "404 Resource Not Found"
        return (code, page)

if __name__ == "__main__":
    webApp = ContentpostApp("localhost", 1235)
